class OpenFlowPacket {
    uint32_t header;
    uint32_t buffer_id;
    uint16_t pkt_len;
    uint16_t in_port;
    uint8_t *pkt_buf;
    uint32_t meta_len;
    uint8_t *meta_buf;
};
