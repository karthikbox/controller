#include <unordered_map>
#include <iostream>
#include <cassert>
#include <thread>
#include <chrono>
#include "zmq.hpp"
#include "sstream"
#include "document.h"
#include "prettywriter.h"
#include "filereadstream.h"
#include "rapidjson.h"
#include <cstdio>
#include <exception>

using namespace std;
class Controller {

    std::unordered_map<std::string, std::string> mappings;
public:
    void HandleFTEntry(rapidjson::Document& document);
    void SendReverseMap(std::string ip);
    void SubscribeToHAProxy();
    void ConnectToFT();
    std::string extract_ip();
};

void Controller::HandleFTEntry(rapidjson::Document &document) {

    //cout << "Inside HANDLEFTENTRY";
    try {
        //if(document.HasMember("out_tuple") == false);
        //    throw "Out_tuple parsing error";

        const char *out_tuple =  (document)["out_tuple"].GetString();
        const char *in_tuple;//cout << "out_tuple:" << out_tuple << endl; 
        if(strcmp("delete_proxy_entry", (document)["cmd"].GetString()) == 0) {
            mappings.erase(out_tuple);    
        } 

        else if(strcmp("add_proxy_entry", (document)["cmd"].GetString()) == 0 ) {
            //if((document).HasMember("in_tuple") == false);
            //    throw "In_tuple parsing error";
            in_tuple =  (document)["in_tuple"].GetString();
            //cout << "in_tuple:" << in_tuple << endl; 
            if(mappings.insert({out_tuple, in_tuple}).second == false)
            {
                cout << "Insertion failed. Key was present";
            }
        }
        else {
            cout << "Unidentified command";
        }

        cout << "inserted in tuple: {[" << out_tuple << "]:[" << in_tuple << "]}" << endl;

    } catch (exception& e) {
    
        cout << "Exception while handling command from HAProxy:" << e.what() << endl;
    }
}

void Controller::SubscribeToHAProxy() {
    //cout<<"sub"<<endl;
    zmq::context_t context (1);
    zmq::socket_t subscriber (context, ZMQ_SUB);
    subscriber.connect("tcp://localhost:5556");

    //  Subscribe to zipcode, default is NYC, 10001
    const char *filter =  "";
    subscriber.setsockopt(ZMQ_SUBSCRIBE, filter, strlen (filter));

    //  Process 100 updates
    int update_nbr;
    long total_temp = 0;
    //for (update_nbr = 0; update_nbr < 10; update_nbr++) {
    while(1) {
        zmq::message_t update;
        //int zipcode, temperature, relhumidity;

        int err=subscriber.recv(&update);
        if(err==-1){
            cout<<"Error Recieving data from HAProxy:"<<" "<<errno<<endl;
            continue;
        }

        // std::istringstream iss(static_cast<char*>(update.data()));
        // char *json = static_cast<char*>(iss.data());
        // cout << iss << endl;
        // iss >> zipcode >> temperature >> relhumidity ;
        // cout << zipcode<< " " << temperature << endl;
        rapidjson::Document document;
        try {
            char *json = static_cast<char*>(update.data());

            cout << "Recieved:" << json <<endl;
            if (document.Parse<0>(json).HasParseError())
                throw "Error Parsing json object recieved";
            if(document.IsObject() == false) 
                throw "Not a object";
            //cout << "Its Object \n";
            //if(document.HasMember("cmd") == false);
            //    throw "Incorrect mapping object recieved.";
            //cout << "CMD: " << (document)["cmd"].GetString() <<  endl;
            //cout << "DONE PARSING"  ;
            // assert(document["hello"].IsString());
        }catch(exception& e) {
            cout << "Exception while parsing mapping recieved from HAProxy:" << e.what() << endl;
        }
        //cout << "Mappings Recieved" << "command" << document["cmd"].GetString()<<endl;
        HandleFTEntry(document);

        // total_temp += temperature;
    }
    // std::cout   << "Average temperature for zipcode '"<< filter
                // <<"' was "<<(int) (total_temp / update_nbr) <<"F"
                // << std::endl;

    for(int i=0;i<5;i++)
    {
        cout << "listening to ha"<<endl;
 //       std::this_thread::sleep_for (std::chrono::seconds(1)); 
    }
}

void Controller::ConnectToFT() {

    for(int i=0;i<5;i++)
    {
        cout << "listening to ft"<<endl;
        //std::this_thread::sleep_for (std::chrono::seconds(1)); 
    }
}

int main(){

    Controller ctrlr;
    cout << "Starting Client..."<<endl;
    //ctrlr.ConnectToFT();
    thread HAProxyThread(&Controller::SubscribeToHAProxy, Controller());

    //thread FTThread(&Controller::ConnectToFT, Controller());

    HAProxyThread.join();
    //FTThread.join();

    return 0;
}
